import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv('titanic.csv')

print(df)

lat_long = df[['Survived', 'Pclass']]
lat, longg = df.Survived, df.Pclass

plt.scatter(longg, lat)

from sklearn.cluster import DBSCAN

X = lat_long.to_numpy()

dbscan_cluster_model = DBSCAN(eps=0.2, min_samples=15).fit(X)
dbscan_cluster_model

df['cluster'] = dbscan_cluster_model.labels_

print(df['cluster'].value_counts())

import plotly.express as px

fig = px.scatter(x=longg, y=lat, color=df['cluster'])

fig.show()